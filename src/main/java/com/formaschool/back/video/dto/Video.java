package com.formaschool.back.video.dto;

import com.formaschool.back.users.dto.UserNamePict;

import lombok.Data;

@Data
public class Video {
	private String peerId;
	private UserNamePict user;
	/**
	 * 0: ask for the list<br>
	 * 1: send infos <br>
	 * 2: call<br>
	 * 3: ? leave
	 */
	private int type;

	private String salonId;
	/** userId */
	private String to;
}
