package com.formaschool.back.video;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formaschool.back.video.dto.Video;

@RestController
@CrossOrigin
@RequestMapping("video")
public class VideoController {

	// ====================================================================================================
	// WebSocket

	@MessageMapping("video.check")
	@SendTo("/vid/public")
	public Video sendMsgWithFile(Video video) {
		return video;
	}
}
