package com.formaschool.back.emojis.dto;

import com.formaschool.back.users.dto.UserNamePict;

import lombok.Data;

@Data
public class EmojiWithFile {
	
	private String id;
	private String name;
	private String teamId;
	private UserNamePict user;
	private String file;
	private String filename;
}
