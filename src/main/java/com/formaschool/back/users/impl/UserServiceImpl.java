package com.formaschool.back.users.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.formaschool.back._crud.CRUDServiceImpl;
import com.formaschool.back._init.InitController;
import com.formaschool.back._utils.Utils;
import com.formaschool.back.files.FileModel;
import com.formaschool.back.files.FileService;
import com.formaschool.back.files.Folder;
import com.formaschool.back.logging.Logger;
import com.formaschool.back.logs.LogService;
import com.formaschool.back.members.Member;
import com.formaschool.back.members.MemberService;
import com.formaschool.back.members.dto.MemberDTO;
import com.formaschool.back.permissions.Permission;
import com.formaschool.back.permissions.PermissionService;
import com.formaschool.back.roles.Role;
import com.formaschool.back.salons.Salon;
import com.formaschool.back.salons.SalonService;
import com.formaschool.back.users.User;
import com.formaschool.back.users.UserRepository;
import com.formaschool.back.users.dto.UserConnect;
import com.formaschool.back.users.dto.UserCreate;
import com.formaschool.back.users.dto.UserCreateWithFile;
import com.formaschool.back.users.dto.UserLocalStorage;
import com.formaschool.back.users.dto.UserNamePict;
import com.formaschool.back.users.services.UserService;

public class UserServiceImpl extends CRUDServiceImpl<User> implements UserService {

	@Autowired
	private InitController init;
	private boolean isInit = false;

	private UserRepository repo;
	private MemberService memberService;
	private FileService fileService;
	private LogService logService;
	private SalonService salonService;
	private PermissionService permissionService;

	private final Logger LOGGER;

	// ====================================================================================================

	public UserServiceImpl(UserRepository repo, Utils utils, MemberService memberService, FileService fileService,
			LogService logService, SalonService salonService, PermissionService permissionService) {
		super(repo, utils);
		this.repo = repo;
		LOGGER = utils.getLogger("UserService");
		this.memberService = memberService;
		this.fileService = fileService;
		this.logService = logService;
		this.salonService = salonService;
		this.permissionService = permissionService;
	}

	// ====================================================================================================

	@Override
	public UserLocalStorage connect(UserConnect connect) {
		initMongo();

		User entity = opt(repo.findByEmail(connect.getEmail()));
		if (!entity.getPassword().equals(convertPwd(connect.getPassword()))) {
			LOGGER.warn("Wrong mdp: " + connect.getEmail());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		LOGGER.info("User connect: " + connect.getEmail());

		UserLocalStorage dto = dto(entity, UserLocalStorage.class);
		dto.setMembers(memberService.findAllByUserId(entity.getId()).stream()
				.map(member -> dto(member, MemberDTO.class)).collect(Collectors.toList()));
		
		List<Permission> permissions = new ArrayList<Permission>();
		findPermissions(dto, permissions);
		dto.setPermissions(permissions);
		return dto;
	}

	/**
	 * Find all permission of a user
	 * @param dto a userLocalStorage 
	 * @param permissions a list of permission which will contain permission
	 */
	private void findPermissions(UserLocalStorage dto, List<Permission> permissions) {
		for (MemberDTO member : dto.getMembers()) {
			List<Salon> salons = getSalonFromMember(member);
			for (Salon salon : salons) {
				List<Role>roles = member.getRoles();
				if(roles != null) {
					for (Role role : member.getRoles()) {
						Permission permission = permissionService.findBySalonIdAndRoleId(salon.getId(), role.getId());
						if(permission !=null)
							permissions.add(permission);
					}
				}
				Permission permission = permissionService.findBySalonIdAndMemberId(salon.getId(), member.getId());
				if(permission!=null)
					permissions.add(permission);
			}
		}
	}
	
	public List<Salon> getSalonFromMember(MemberDTO member){
		return salonService.findAllSalonOfTeam(member.getTeam().getId());
	}

	// ====================================================================================================
	// Add

	@Override
	public UserNamePict getUserNamePictById(String id) {
		return dto(opt(repo.findById(id)), UserNamePict.class);
	}

	@Override
	public List<UserNamePict> getUserNotInTheTeam(String teamId) {
		List<User> users = this.repo.findAll();
		List<User> userIntheTeam = getUserByTeamId(teamId);
		return users.stream().filter(user -> !userIntheTeam.contains(user)).map(user -> dto(user, UserNamePict.class))
				.collect(Collectors.toList());
	}

	@Override
	public List<User> getUserByTeamId(String teamId) {
		List<Member> membersInTheTeam = this.memberService.findByTeamId(teamId);
		List<User> userIntheTeam = new ArrayList<User>();
		for (Member member : membersInTheTeam) {
			userIntheTeam.add(member.getUser());
		}
		return userIntheTeam;
	}

	// ====================================================================================================
	// Create

	@Override
	public User addUser(UserCreate userCreate, String idAddedBy) {
		User user = dto(userCreate, User.class);
		LOGGER.info("Create user: " + user);
		user.setCreation(LocalDate.now());
		this.logService.addUserLog(user.getFirstname(), user.getLastname(), idAddedBy);
		return this.repo.save(user);
	}

	@Override
	public User saveWithFile(UserCreateWithFile user, String idAddedBy) {
		// TODO BirthDate + Phone
		User entity;
		if (user.getFile() != null) {
			FileModel file = fileService.upload(Folder.USERS, user.getFilename(), user.getFile());
			entity = new User(user.getFirstname(), user.getLastname(), user.getPassword(), user.getEmail(), file, null,
					"0612345678", LocalDate.now(), false);
		} else {
			entity = new User(user.getFirstname(), user.getLastname(), user.getPassword(), user.getEmail(), null, null,
					"0612345678", LocalDate.now(), false);
		}
		this.logService.addUserLog(user.getFirstname(), user.getLastname(), idAddedBy);
		return repo.save(entity);
	}
	// ====================================================================================================

	/** Transform the password to save it in the DB */
	public String convertPwd(String pwd) {
		// TODO [Improve]
		// TODO Call on save/update user
		return pwd;
	}

	// ====================================================================================================

	/** Init the database on the first connection */
	public void initMongo() {
		if (isInit)
			return;
		try {
			init.drop();
			init.init();
			isInit = true;
		} catch (ResponseStatusException e) {
		}
	}

	@Override
	public User updateAdmin(String userId, Boolean isAdmin) {
		User user = opt(repo.findById(userId));
		user.setIsAdmin(isAdmin);
		return this.save(user);
	}

	@Override
	public void deleteUser(String userId) {
		this.memberService.deleteMemberFromUserId(userId);
		this.repo.deleteById(userId);
	}
}
