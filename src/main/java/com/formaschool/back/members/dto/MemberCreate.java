package com.formaschool.back.members.dto;

import com.formaschool.back.teams.dto.TeamNamePict;
import com.formaschool.back.users.dto.UserNamePict;

import lombok.Data;

@Data
public class MemberCreate {
	
	private TeamNamePict team;
	private UserNamePict user;

}
