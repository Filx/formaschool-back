package com.formaschool.back.salons.dto;

import lombok.Data;

@Data
public class SalonCreate {

	private String name;
	private String desc;
	private String teamId;

}
