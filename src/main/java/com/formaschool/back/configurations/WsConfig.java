package com.formaschool.back.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

@Configuration
@EnableWebSocketMessageBroker
public class WsConfig implements WebSocketMessageBrokerConfigurer {

//	private final String front = "*";
	private final String front = "http://formaschool.ddns.net";
//	private final String front = "http://0.0.0.0";
//	private final String front = "http://localhost:4200";

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/wsMessages").setAllowedOrigins(front).withSockJS();
		registry.addEndpoint("/wsVideo").setAllowedOrigins(front).withSockJS();
		registry.addEndpoint("/wsRights").setAllowedOrigins(front).withSockJS();
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker("/msg", "/vid", "/rights");
		registry.setApplicationDestinationPrefixes("/app");
	}

	@Override
	public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
		registration.setMessageSizeLimit(10_000_000); // default : 64 * 1024
		registration.setSendTimeLimit(20 * 10000); // default : 10 * 10000
		registration.setSendBufferSizeLimit(3 * 512 * 1024); // default : 512 * 1024
	}
}
