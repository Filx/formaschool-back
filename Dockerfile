FROM maven:3.8.1-openjdk-16

COPY src/ src
COPY pom.xml pom.xml

RUN mvn install -Dmaven.test.skip=true

CMD java -jar "./target/back-0.0.1-SNAPSHOT.jar"


#FROM maven:3.8.1-openjdk-16 AS BUILDER
#COPY src/ src
#COPY pom.xml pom.xml
#RUN mvn package -Dmaven.test.skip=true

#FROM openjdk:11.0.11-jdk-oracle
#COPY --from=BUILDER target/*.jar target/app.jar
#CMD java -jar target/app.jar
